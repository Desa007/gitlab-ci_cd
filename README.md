# Gitlab CI_CD



## Getting started
To run your first pipeline in Gitlab-ci you need to add file `.gitlab-ci.yml` into main level of your project.

To set custom [path](https://docs.gitlab.com/ee/ci/pipelines/settings.html#specify-a-custom-cicd-configuration-file) for `.gitlab-ci.yml`
***Settings -> CI/CD -> General pipelines -> CI/CD configuration file***

## Main Gitlab-CI sections
### Stages
This section is a list and order of stages in pipeline when to run a Job

Example:
```yaml
stages:
  - check
  - build
  - tag-latest
  - deploy
```

### Jobs
A number of actions that define what to do.
Hidden Job start from *"."* -> .hidden_job. This need in 2 cases:
 - temporarily disable a job without deleting it from the configuration file
 - for reusable configuration with **extend** or **YAML anchors**


Example 1:
```yaml
build_check:
  stage: check
  when: manual
  except:
    - dev
    - master
  environment: dev
  variables:
    MAKE: make -f .docker/.env/ci-build/Makefile
  script:
    - cp api/config/autoload/local.php.dist api/config/autoload/local.php
    - cat client/$CI_ENVIRONMENT_NAME.env > client/.env
    - cat control/$CI_ENVIRONMENT_NAME.env > control/.env
    - ${MAKE} build-check
  resource_group: dev
  allow_failure: false
```
Example 2:
```yaml
.job_template: &job_configuration  # Hidden yaml configuration that defines an anchor named 'job_configuration'
  image: ruby:2.6
  services:
    - postgres
    - redis

test1:
  <<: *job_configuration           # Merge the contents of the 'job_configuration' alias
  script:
    - test1 project

test2:
  <<: *job_configuration           # Merge the contents of the 'job_configuration' alias
  script:
    - test2 project
      
rspec:
  extends: .job_template
  script:
    - test3 project
```

Full [configuration options](https://docs.gitlab.com/ee/ci/yaml/) list you can find in official Gitlab documantation.

In this example we can find next configuration options:

- ***build_check*** - Name of Job
- ***stage: check*** - Name of Stage when/where this Job will run
- ***when: manual*** - condition when to run this Job. The default value ```when: on_success```
  - ```on_success``` (default): Run the job only when all jobs in earlier stages succeed or have `allow_failure: true`. - manual: Run the job only when triggered manually.
  - ```always:``` Run the job regardless of the status of jobs in earlier stages.
  - ```on_failure:``` Run the job only when at least one job in an earlier stage fails.
  - ```delayed:``` Delay the execution of a job for a specified duration.
  - ```never:``` Don’t run the job.
- [***except:***](https://docs.gitlab.com/ee/ci/yaml/) control when jobs are NOT created. You can use `only` and `except` to control when to add jobs to pipelines:
  - Use ```only``` to define when a job runs.
  - Use ```except``` to define when a job does not run.
- [***environment:***](https://docs.gitlab.com/ee/ci/environments/index.html) environments describe where code is deployed.
- [***variables:***](https://docs.gitlab.com/ee/ci/variables/) list of variables for this Job
  - Variables in the .gitlab-ci.yml file
  - Project CI/CD variables
  - Group CI/CD variables
  - Instance CI/CD variables
- [***script:***](https://docs.gitlab.com/ee/ci/yaml/#script) specify commands for the runner to execute for Job
- [***resource_group:***](https://docs.gitlab.com/ee/ci/resource_groups/) - Limit job concurrency. For example, if multiple jobs that belong to the same resource group are queued simultaneously, only one of the jobs starts. The other jobs wait until the resource_group is free.
- ***allow_failure:*** - determine whether a pipeline should continue running when a job fails:
  - To let the pipeline continue running subsequent jobs, use allow_failure: true.
  - To stop the pipeline from running subsequent jobs, use allow_failure: false.
- ***before_script:*** - define an array of commands that should run before each job’s script commands, but after artifacts are restored. To skip before_script commands for some Job you need add empty brackets
  ```yaml
  build_check:
    stage: check
    when: manual
    before_script: []
  ...
  ```
- [***services:***](https://docs.gitlab.com/ee/ci/services/) - defines a Docker image that runs during a job linked to the Docker image that the image keyword defines. This allows you to access the service image during build time
- [***artifacts:***](https://docs.gitlab.com/ee/ci/yaml/#artifacts) - specify which files to save as job artifacts
```yaml
  artifacts:
    # Paths are relative to the project directory ($CI_PROJECT_DIR) and can’t directly link outside it.
    paths:
      - assets/public
    # How long job artifacts are stored
    expire_in: 1 day
```

---
[.gitlab-ci.yml examples](https://gitlab.com/gitlab-org/gitlab/-/tree/master/.gitlab/ci)